﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disemvoweler
{
    class Program
    {
        static void Main(string[] args)
        {
            Disemvoweler("Nickleback is my favorite band. Their songwriting can't be beat!");
            Console.WriteLine();
            Disemvoweler("How many bears could bear grylls grill if bear grylls could grill bears?");
            Console.WriteLine();
            Disemvoweler("I'm a code ninja, baby. I make the functions and I make the calls.");
            Console.WriteLine();

            // End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        /// <summary>
        /// Remove all vowels from a input string and print/return the disemvowel'd list
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Disemvoweler(string input)
        {
            input = input.ToLower();
            //init new blank outputs
            string inputWithoutVowels = String.Empty;
            string removedVowels = String.Empty;

            //iterate through each character and add consonants to inputWithoutVowels and vowels to removedVowels
            foreach (var currentChar in input)
            {
                if ("bcdfghjklmnpqrstvwxyz".Contains(currentChar))
                {
                    inputWithoutVowels += currentChar;
                }
                if ("aeiou".Contains(currentChar))
                {
                    removedVowels += currentChar;
                }
            }
            
            // Write out the various string results
            Console.WriteLine("Original: {0}", input);
            Console.WriteLine("Disemvoweled: {0}", inputWithoutVowels);
            Console.WriteLine("Vowels Removed: {0}", removedVowels); 
            // Return the Disemvoweled string as well for testing
            return inputWithoutVowels;
        }
    }
}
